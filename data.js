let obj = {
  columns: ['Name', 'Email', 'Phone', 'Job'],
  data: []
};

class Table {
  constructor(obj){
    this.obj = obj;
  }

  createHeader(objHeader){
    let open = '<thead class="table-light"><tr>';
    let close = '</tr></thead>';
    objHeader.forEach(element => open+=`<th>${element}</th>`);

    return open + close;
  }

  createBody(objBody){
    let open = '<tbody>';
    let close = '</tbody>';
    objBody.forEach(element => {
      open += `
        <tr>
          <td>${element[0]}</td>
          <td>${element[1]}</td>
          <td>${element[2]}</td>
          <td>${element[3]}</td>
        </tr>
      `;
    });

    return open + close;
  }

  render(container){
    let table = '<table class="table table-bordered table-hover">'+
      this.createHeader(this.obj.columns) +
      this.createBody(this.obj.data) +
    '</table>';
    
    container.innerHTML = table;
  }
}

export {obj, Table};