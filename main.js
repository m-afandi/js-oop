import {obj, Table} from './data.js';

let content = document.getElementById('content');
let button = document.getElementById('button');

let clearUp = (nama, email, phone, job) => {
  nama.value = "";
  email.value = "";
  phone.value = "";
  job.value = "";
  return alert("Data Berhasil Dimasukan !");
}

button.addEventListener('click', function() {
  let nama = document.getElementById('nama');
  let email = document.getElementById('email');
  let phone = document.getElementById('phone');
  let job = document.getElementById('job');

  obj.data.push([nama.value, email.value, phone.value, job.value]);
  let newTable = new Table(obj);

  newTable.render(content);
  return clearUp(nama, email, phone, job);
});